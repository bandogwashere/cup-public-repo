/*
        Basic retexture config template for CUP assets
        by audiocustoms
*/



class CfgPatches
{
        class ABC_AirVehicles_412

        /*
                PREFIX_CATEGORY_MODEL

                PREFIX:         Your artist prefix. it usualy contains 3 letters and often is
                                an abbreviation of your nickname.
                CATEGORY:       The category that the asset you retexture is in.
                                Examples are
                                `AirVehicles`, `TrackedVehicles`,
                                `WaterVehicles`, `Weapons`,
                                `WheeledVehicles` and `Creatures`
                MODEL:          For easy identification, select the name that like our assets are called

                Example:        ABC_AirVehicles_412     (retexture of the Bell 412)

                TIP:            Use the same class name as the folder this file is in
        */

        {
                units[]={};
                weapons[]={};
                requiredVersion=0.1;
                requiredAddons[]={"CUP_AirVehicles_LoadOrder"};

                /*
                        units[]                 Will be filled by pboProject
                        weapons[]               Will be filled by pboProject
                        requiredVersion         No change needed
                        requiredAddons[]        This will define the dependency and where to find the original files.
                                                For category AirVehicles use `CUP_AirVehicles_LoadOrder`
                                                For category Creatures use `CUP_Creatures_People_LoadOrder`
                                                For category TrackedVehicles use `CUP_TrackedVehicles_LoadOrder`
                                                For category WaterVehicles use `CUP_WaterVehicles_LoadOrder`
                                                For category Weapons use `CUP_Weapons_LoadOrder`
                                                For category WheeledVehicles use `CUP_WheeledVehicles_LoadOrder`
                */
        };
};

class cfgVehicles       //For Weapons use `class cfgWeapons`
{
        class CUP_C_412;
        /*
                Use the original classname of the asset you want to retexture here.
        */

        class ABC_C_412_CIV_COLORMAP: CUP_C_412
        /*
                PREFIX_SIDE_MODEL_FACTION_TEXTURENAME

                The same principle as in cfgPatches.
                PREFIX:         Your artist prefix.
                SIDE:           Define which side this aset is for.
                                O = OPFOR
                                B = BLUFOR
                                I = IND
                                C = CIV
                MODEL:          The model you are using.
                FACTION:        The faction that will use this asset.
                TEXTURENAME:    Use the name of your texture or an abbreviation of it.
        */

        {
                scope=2;
                /*
                Visibility of this class in the game. 0 means not at all, 1 means it can only be called via script,
                and 2 means it is visible / selectable in the editor.
                */

                scopeCurator=2;
                /*
                Basically the same as for `scope=X`but of Zeus
                */

                displayName="Bell 412 (Colormap)";
                /*
                The name the new class should be displayed with in the editor / Zeus
                */

                side=3
                /*
                Defines on which side this asset can be found.
                0 is OPFOR
                1 is BLUFOR
                2 is Independent
                3 is Civilian
                */

                faction="CIV_F"
                /*
                Defines which faction uses this asset.
                For more info see: https://community.bistudio.com/wiki/faction
                */

                crew="C_man_pilot_F";
                /*
                What unit is supposed to sit in the pilot and copilot seat.
                */

                hiddenSelectionsTextures[]=
                {
                        "ABC_CUP_Retextures\ABC_AirVehicles_412\skins\color_main.paa",
                        "ABC_CUP_Retextures\ABC_AirVehicles_412\skins\color_ext.paa",
                        "ABC_CUP_Retextures\ABC_AirVehicles_412\skins\color_ext1.paa"
                };
                /*
                This is where the magic happens.
                Check the original asset how many terxtures it uses.
                Use the full paths to your textures, separate them with commas. make sure the last texture
                has no comma!
                */
        };
};

